<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sample Page</title>
    <link rel="stylesheet" href="login.css">
    <link rel="stylesheet" href="assests/css/bootstrap.min.css">
</head>

<body>
    
    <div class="bg-image"></div>

    <div class="bg-text">
        <h2>Blurred Background</h2>
        <h1 style="font-size:50px">I am John Doe</h1>
        <p>And I'm a Photographer</p>
    </div>

    <script src="assests/js/bootstrap.min.js"></script>
</body>

</html>