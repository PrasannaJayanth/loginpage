<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>
    <link rel="stylesheet" href="login.css">
    <link rel="stylesheet" href="assests/css/bootstrap.min.css">
</head>

<body>
    <header class="header py-4 px-4">
        <img src="images/image 3.png" class="img-fluid">
    </header>
    <div class="container">
        <div class="row  py-5 px-3 ">
            <div class="col-lg-6 col-md-6 col-12 align-self-center    ">
                <img src="images/Group.png" class="img-fluid " width="550px">
            </div>
            <div class="col-lg-6 col-md-6 col-12 justify-content-center text-center ">

                <form class="text-center">
                    <h3>Login</h3>
                    <div class="form-group d-inline-flex  py-3 ">
                        <input type="email" class="form-control " placeholder="User Name" id="email">
                    </div>
                    <div class="form-group d-inline-flex py-3">

                        <input type="password" class="form-control" placeholder="Password" id="pwd">
                    </div>
                    <div class="row mb-4">
                        <div class="col-lg-7 col-md-5 col-6 ">
                            <div class="checkbox">
                                <label><input type="checkbox"> Remember me</label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-7 col-6">
                            <a href="#" class="text-decoration-none">Forgot Password? </a>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block p-2 mb-4">Submit</button>
                    </div>
                    <div class="form-group  ">
                        <p>Didn't have an account? <a href="# " class="text-decoration-none">Sign Up now</a></p>
                        <p class="text-muted ">OR</p>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn1 btn-light border btn-block p-2 mb-4">
                            <img src="images/Google_Icons-09-512.webp" width="40" height="30" class="d-inline-block align-text-center">
                            <span>Sign Up With Google</span>
                        </button>
                    </div>





                </form>
            </div>
        </div>
    </div>

    <script src="assests/js/bootstrap.min.js"></script>
</body>

</html>